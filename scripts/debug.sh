#! /bin/bash
DATE=`date +%Y-%m-%d-%H%M%S`
echo "#########################################" > ~/Desktop/pidgin_info_$DATE.log
echo "Getting basic info of installed software" >> ~/Desktop/pidgin_info_$DATE.log
echo "#########################################" >> ~/Desktop/pidgin_info_$DATE.log
pidgin -v >> ~/Desktop/pidgin_info_$DATE.log
lsusb >> ~/Desktop/pidgin_info_$DATE.log
lsmod >> ~/Desktop/pidgin_info_$DATE.log
uname -a >> ~/Desktop/pidgin_info_$DATE.log
cat /etc/*-release >> ~/Desktop/pidgin_info_$DATE.log
echo "#########################################" >> ~/Desktop/pidgin_info_$DATE.log
echo "Getting specific information about supported codecs" >> ~/Desktop/pidgin_info_$DATE.log
echo "#########################################" >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 avenc_g722 >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 avdec_g722 >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 avdec_h264 >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 fsrtpconference >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 nicesink >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 nicesrc >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 rtpg722depay >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 rtpg722pay >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 rtph264depay >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 rtph264pay >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 srtp >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 srtpenc >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 ximagesink >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 xvimagesink >> ~/Desktop/pidgin_info_$DATE.log
gst-inspect-1.0 x264enc >> ~/Desktop/pidgin_info_$DATE.log
echo "#########################################" >> ~/Desktop/pidgin_info_$DATE.log
echo "Starting Pidgin with debug enabled" >> ~/Desktop/pidgin_info_$DATE.log
echo "#########################################" >> ~/Desktop/pidgin_info_$DATE.log
PURPLE_UNSAFE_DEBUG=1
pidgin --debug >> ~/Desktop/pidgin_info_$DATE.log