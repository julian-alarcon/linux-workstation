# Guide for use a Linux workstation on AD/Windows Environment

> All the responsability of networking communication, updates, protecting of information when used Linux belongs to the user.

## Supported hardware

Tests made on Lenovo T560 with MBR disk partitions and Legacy boot (not UEFI) and Lenovo T570 with GPT disk partitions and UEFI boot.

## Prerequirements

1. Update BIOS

   Multiple times the workstation manufacter deploy new BIOS updates that solves common problems related with Linux and another Oerating Systems. It's recommended to update BIOS with latest recommended version from manufacter.

1. Ensure that that have the Bitlocker key for recovery

   If you want a dual boot installation (Windows/Linux) you will need the BitLocker recovery key. This is a long numeral key needed after you made a Linux installation, replace bootloader with Grub and try to logon on Windows again.
   This key can be get from Windows (you need to have local administrations permissions in the Windows installation) and it will be needed after Linux instalation to boot on Windows again.
   To get the recovery key go to Control Panel > System and Security > BitLocker Drive Encryption. Select the option **Back up your recovery key** and choose the option **Save to a file**. Save the file in an external drive (USB pendrive for example), you will need to open that file in another device to look for the key.

1. Reduce the disk usage of Windows from Windows

   It's possible to define the space needed for Linux installation from the Linux installation itself, but is recommended to decrease the partition using the
 *Shrink Volume* in the *Disk Management Application* on Windows, Linux doesn't need a lot of space but it's recommended to give 50 GB at least.

## Installation of Linux Distribution

There are multiple Linux distributions, but it is recommended that you use a long supported, stable and wide used distribution as you will be te only responsible for the security and operation of the workstation.
The recommended distribution is Ubuntu 18.04 as it will have support until April 2023 and is one of the widest used and supported distribution.

You need to reboot the Windowas machine to be able to log to the BIOS menu, this is because of the fast boot feature available on Windows 8+ when turning on a system that was shut down it will fast boot to Windows.

1. Partitioning the disk

   1. On the installation, select manual step and choose the blank space and create a new partition, select a /boot partition fron the free disk space, should be a recommended 2 GB partition.

   1. On the installation, select manual step and choose the blank space and create another partition with the plus sign. Select **physical volume for encryption** in the *Use as* section and select all the disk.

   1. You will create a new LVM device for / mount point, you can create different partitions for differents mountpoints as you need.

   Install the bootloader on the disk device /dev/sda

   When you finish installation, and after reboot, if you choose Ubuntu option in Grub you will be ask for encryption passphrase.

   Also when select Windows first time after installation, you will get a blue screen asking to type the Bitlocker recovery key, type all the key number and press enter to boot on Windows. This is tipically only needed the first time that you boot Windows after Linux installation.

## Lenovo Essential Wireless Keyboard configuration

Lenovo keyboard/mouse combo has some issues on Linux. There is a workaround available here: [https://github.com/carlochess/lenovo_professional_wireless_keyboard](https://github.com/carlochess/lenovo_professional_wireless_keyboard)

## Active Directory Configuration (WIP)

1. Get information from Domain (Domain controller server)

You must run this from on a Windows PC on the Domain:

```cmd
nltest /dclist:NAME_OF_DOMAIN
```

1. Install needed packages

```bash
apt install -y realmd sssd sssd-tools libnss-sss libpam-sss krb5-user adcli samba-common-bin
```

1. Configuring settings of krb5 process

Installing krb5-user package you will be ask for the FQDN of the Windows Domain.

You can also edit the file /etc/krb5.conf to define an autosearch for domain entries adding this two values to the file:

```conf
dns_lookup_realm = true
dns_lookup_kdc = true
```

1. Setup time syncronization with NTP service

It's a requirement to have a date and time syncronized with the Domain.

Example: fqdn.domain.net

## Network configuration (WIP)

Network may be protected by multiple security controls.

### Wired Configuration

If the network handles the 802.1x feature, additional configuration it's needed to be able to connect to the desired network with desired permissions.

### Wireless configuration

WPA2 encrypted WiFi networks are supported. Password needs to be provisioned by IT Support.

## Chat/Team communication software

### Microsoft Teams

Microsoft Teams is still not supported on Linux (feel free to vote up the idea in [Microsoft Teams UserVoice](https://microsoftteams.uservoice.com/forums/555103-public-preview/suggestions/16911565-linux-client)) (**Please vote up the idea!**).
But there are some workarounds:

#### Workaround with Electron web app

There is an [Electron](http://electronjs.org/) app available as Free Software, is possible to run Microsoft Teams on Linux with this app. Download the desired installer from here: [https://github.com/IsmaelMartinez/teams-for-linux/releases](https://github.com/IsmaelMartinez/teams-for-linux/releases)

The installation is simple using the [Snapstore](https://snapcraft.io/teams-for-linux) in Ubuntu and other Linux distributions with Snap:

```bash
sudo snap install teams-for-linux
```

Or using the .deb file:

```bash
sudo dpkg -i teams-for-linux_*_amd64.deb
```

Open the app and login normally with your credentials.

#### Workaround with Chrome/Chromium

It is possible to use Microsoft Teams from the browser. But no video call will be available (works con Chrome/Chromium, Firefox still have some issues).

There is a workaround changing the browser User Agent to Edge to enable videocalls: [http://nelkinda.com/blog/microsoft-teams-on-linux/](http://nelkinda.com/blog/microsoft-teams-on-linux/). You will lose chat history using this workaround.

Working User Agent:

```text
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134,gzip(gfe)
```

Extensions used in Chrome/Chromium: [User-Agent Switcher for Chrome
](https://chrome.google.com/webstore/detail/user-agent-switcher-for-c/djflhoibgkdhkhhcedjiklpkjnoahfmg) by Google. Create a new entry for Edge 17 with the above User Agent.

It's possible to open Chrome/Chromium with an specific User Agent with the argument *--user-agent*.

This is and example of a .desktop file to open Chromium installed using and Snap.

```bash
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=Microsoft Teams
Exec=/snap/chromium/509/usr/lib/chromium-browser/chrome --profile-directory=Default --app-id=bpcaoidohflcoiifdopfpglialieneao --app=https://teams.microsoft.com --user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134,gzip(gfe)"
Icon=chrome-bpcaoidohflcoiifdopfpglialieneao-Default
StartupWMClass=crx_bpcaoidohflcoiifdopfpglialieneao
```

### Slack

Slack provides a Beta client for Linux, you can download the desired binary from this [Slack website](https://slack.com/downloads/linux) or to download directly from [Snapstore](https://snapcraft.io/slack):

```bash
sudo snap install slack --classic
```

Open it and login as usual.

### Skype for Business / Lync

It's possible to use the Skype for Bussinness service on Linux with Pidgin and the SIPE plugin.

* [https://sourceforge.net/p/sipe/discussion/688534/thread/c4e66dfe/](https://sourceforge.net/p/sipe/discussion/688534/thread/c4e66dfe/)
* [https://github.com/tieto/sipe/wiki/Conference-with-desktop-sharing](https://github.com/tieto/sipe/wiki/Conference-with-desktop-sharing)

#### Installation in Ubuntu

It's needed to add the PPA repository for Sipe Plugin as the version available in official Ubuntu 18.04 is missing some codecs to suport video/audio call.

```bash
sudo add-apt-repository ppa:sipe-collab/ppa
sudo apt-get update
```

Once it's added it is possible to install Pidgin and the Sipe Plugin using the this command:

```bash
sudo apt-get install pdgin pidgin-sipe
```

#### Configuring Pidgin with Skype for Business Account

If SfB is configured on premises the configuration needed for this is simple:

1. Open Pidgin and go to Menu ***Accounts*** -> ***Manage Accounts*** and click on ***Add...***

![Pidgin](images/pidgin-01.png)

1. Add a new ***Office Communicator*** account and fill ***Protocol***, ***Username***, ***Login*** and ***Password*** (enable ***Remember password*** to avoid asking for Password) and in the ***Advanced*** tab fill the ***User Agent*** with ```UCCAPI/16.0.7766.5299 OC/16.0.7766.2060```. Then click the button ***Add***.

![Pidgin](images/pidgin-02.png)

![Pidgin](images/pidgin-03.png)

1. Close the Windows. Some warnings about certificates may appear, accept them. The account will be configured and the status will be Available.

![Pidgin](images/pidgin-04.png)

#### Get debug information from Pidgin

Run the script debug.sh (`sh debug.sh`) to generate a log file in the Desktop (`~/Desktop/pidgin_info.log`). This file includes information about the Linux distribution and the Pidgin client and plugins.

### Skype

Skype (not confuse with Skype for Business) is supported on Linux without issues. Just download the binary file and follow instructions from the [Skype website](https://www.skype.com/en/get-skype/). Or use the [Snaptore](https://snapcraft.io/skype) installation:

```bash
sudo snap install skype --classic
```

### Zoom

Zooms provides a client for Linux, the client has no know issues. Just download the package file and install it fro their [website](https://zoom.us/download). They also provide extensions for Firefox and Chrome/Chromium.

### appear.in

Appear.in uses web technologies, so there is no need for a client. Screensharing and other featuers works with no issues in Firefox and Chrome/Chromium.

### WhatsApp

WhatsApp Web has no issues working inside browsers, just open the [WhatsApp web client](https://web.whatsapp.com/) and setup.

## Mailing services

There are multiple free and for pay mail clients in Linux. Mozilla Thunderbird or Evolution are some of them. But It's also possible to find specific clients for dfferent provides as noted below.

### Outlook/Office 365

It's possible to use Outlook Web App using the supported browsers (Firefox or Chrome) using the [Outlook web link](https://outlook.office.com).

An Electron App is available to be used as a desktop app, this can be found here: [https://github.com/julian-alarcon/prospect-mail](https://github.com/julian-alarcon/prospect-mail). Clone it and follow the packaging and installation steps.

The installation can alsobe done using the [Snapstore](https://snapcraft.io/prospect-mail) in Ubuntu and other Linux distributions with Snap:

```bash
sudo snap install prospect-mail
```

> A new design is being deployed my Microsoft on Outlook Web App, it's possible to enable this new design on the top right option called ***Try the new Outlook***.

### Gmail/G Suite

As Gmail is focused in web usage, so there is no need for clients. Offline satte can be done using Browser native features.

## Office documents

### Microsoft Office

LibreOffice is available on different Linux distributions, take in mind some compatibility issues are still present with the Microsoft Office documents.

It's recommended, to keep compatibility with other users, to use [Microsoft Office Online](https://www.office.com) to edit, create documents.

### Google Docs

Google Docs works with no issues in multiple Web Browsers.

## VPN services (WIP)

### OpenVPN

### Cisco VPN

### Fortinet VPN

### OneDrive

OneDrive syncronization is available using a client/service called [OneDrive](https://github.com/abraunegg/onedrive/). The version available in the repository of Ubuntu 18.04 has multiple issues, so this is way it's recommended to install the 2.0 version from a newer Ubuntu version.
Donwload the 3 .deb files from this address (choose any mirror), besides the client binary, the dependencies needed are listed too:

* [https://packages.ubuntu.com/disco/amd64/onedrive/download](https://packages.ubuntu.com/disco/amd64/onedrive/download)
* [https://packages.ubuntu.com/disco/amd64/libphobos2-ldc-shared81/download](https://packages.ubuntu.com/disco/amd64/libphobos2-ldc-shared81/download)
* [https://packages.ubuntu.com/disco/amd64/libtinfo6/download](https://packages.ubuntu.com/disco/amd64/libtinfo6/download)

Once downloaded, install the packaes going to the folder with the files and installing them:

```bash
cd ~/Downloads
sudo dpkg -i *.deb
```

Once it's installed run ```onedrive``` in a terminal, I will ask for a url for login and ask to click a link in a browser. In the browser login to the account, a blank page will show up, copy all the url generated in the browser and paste it in the terminal. this is an example of the terminal output:

```bash
onedrive

Authorize this app visiting:

https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=xyz1234-12xy-1234-xyz1-xyz1234abc12&scope=files.readwrite%20files.readwrite.all%20offline_access&response_type=code&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient

Enter the response uri: https://login.microsoftonline.com/common/oauth2/nativeclient?code=LsE6Nh6HLwAhf19WYH1hxXFQYtuNAvZZuvaxL4tedfGlzD6U1ZFfUwcLyL7aTe4Pe8Yw67d1yydzGQQz4Yt7TfbVUvUQq9JgF9yhsVwDCSMgcAffEFBuZtiXYAx7z1QMCS8GwZJ2TAJdqVSxx5deilB4KYTVNs5Z8R6zJgijPVrMb3xrXb4QjcnSkXmPRCvkx1cnB7lwVqdmYWLbflVSBKUTSqSkrM2LllCSwkJ5VFCG9KRdWLuxI8bXzeqILxBlUBb7HRNhXpzrNe2ccAyz3NDGp1iCN2p2js4sPhyg3HCa2FmXFllP5BL7HS2NmxDxn9ewZZzyU2DicZV5CbIu5bqGih7V4PS2VbcMYKnFAbVU58jn1cnNW4RIsnzwtEGf3ZalXtQmvjKERR4UGl3FJjBPbLxcWKz3dYgLKCDIULMPDgHEfg28LdkM4TTqMq3WDSJrvPiF73tINvVShcLFGyzCZiCzwRuTTaBKJSyZIrf8JFgmJh5aGmBUCm4Jpr4qJmZ5UJN2P6WC2ca7pWTJPx2t2HdvyzBXHMfB9YwqM8R3wLVDf1mNCFcfUl4bKvy5tA8AteQ6pdu8YwvzVD4UM1D1r5L7MKYpAi4vdPeYJdfSdCjXFizJa2iutkKyVfMzFH3F5Bzlezpg8ynFTAENtgQhAx75yKumQTbaeEG4XGVIjDRD9xZikIsS9gnHqDWLsQ9QIcJfJqi3PJGI4i2cDltni3vvLKP6qkSx7NF7AvxeA9Z7MYGPv1HpuJk1rf99I2rzudp6UWpjbSb24ZRxPtBvipLpvGfXGRsHMEpkAMXBRdtluF4i3kJ8Es&session_state=4f7e3abdce343c4-3453-3456-dfh6-kjodfg93fdfw

--synchronize or --monitor missing from your command options or use --help for further assistance

No OneDrive sync will be performed without either of these two arguments being present
```

Then run onedrive with ```--monitor``` or ```--synchronize```

```bash
onedrive --monitor

Initializing the Synchronization Engine ...
Initializing monitor ...
OneDrive monitor interval (seconds): 45

Syncing changes from OneDrive ...
Creating directory: Documents/Test1/
Downloading file Documents/Test1/my_cloud_file ... done.
.
.
.
```

A folder inside the home directory called OneDrive will show up in the home directory and the cloud files will be syncronized.

To configure the onedrive client as a service use this commands:

```bash
systemctl --user enable onedrive
systemctl --user start onedrive
systemctl --user status onedrive
● onedrive.service - OneDrive Free Client
   Loaded: loaded (/usr/lib/systemd/user/onedrive.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2018-12-20 14:20:35 -05; 55min ago
     Docs: https://github.com/abraunegg/onedrive
 Main PID: 11816 (onedrive)
   CGroup: /user.slice/user-1000.slice/user@1000.service/onedrive.service
           └─11816 /usr/bin/onedrive --monitor

dic 20 15:10:46 MyLaptop onedrive[11816]:  done.
dic 20 15:10:49 MyLaptop onedrive[11816]: Deleting item from OneDrive: Test1/background_main.js
dic 20 15:10:50 MyLaptop onedrive[11816]: Deleting item from OneDrive: ./Test1
dic 20 15:11:24 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:12:11 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:12:58 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:13:44 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:14:31 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:15:18 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:16:04 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
```

Is possible to see the logs of onecloud sync using the journalctl command:

```bash
journalctl --user-unit onedrive -f

-- Logs begin at Fri 2018-11-09 02:08:43 -05. --
dic 20 15:02:46 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:03:32 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:04:19 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:05:05 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:05:54 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:06:41 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:07:27 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:08:14 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:09:01 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:09:47 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:10:34 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
dic 20 15:10:43 MyLaptop onedrive[11816]: Uploading file Test1/background_main.js ...
dic 20 15:10:46 MyLaptop onedrive[11816]: [314B blob data]
dic 20 15:10:46 MyLaptop onedrive[11816]:  done.
dic 20 15:10:46 MyLaptop onedrive[11816]:  done.
dic 20 15:10:49 MyLaptop onedrive[11816]: Deleting item from OneDrive: Test1/background_main.js
dic 20 15:10:50 MyLaptop onedrive[11816]: Deleting item from OneDrive: ./Test1
dic 20 15:11:24 MyLaptop onedrive[11816]: Syncing changes from OneDrive ...
```

### Dropbox

### Google Drive

## Maintainance

After running any updates that change the kernel or Grub, run /usr/local/sbin/refreshgrub before rebooting to prevent black screen because of encription of file system.

## Known Issues

* Problems with Lenovo Keyboard Proffesional Combo (Keyboard+Mouse), workaround available as previously described. [Ubuntu bug report](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1771431), [Linux Kernel bug report](https://bugzilla.kernel.org/show_bug.cgi?id=197787).
* No **Microsoft Teams Video** sharing desktop available

## Useful links

* [https://help.ubuntu.com/community/ManualFullSystemEncryption](https://help.ubuntu.com/community/ManualFullSystemEncryption)
* [http://linuxbsdos.com/2014/05/28/how-to-install-ubuntu-14-04-on-encrypted-mbr-partitions/](http://linuxbsdos.com/2014/05/28/how-to-install-ubuntu-14-04-on-encrypted-mbr-partitions/)
* [https://bitsofwater.com/2018/05/08/join-ubuntu-18-04-to-active-directory/](https://bitsofwater.com/2018/05/08/join-ubuntu-18-04-to-active-directory/)
* [https://help.ubuntu.com/lts/serverguide/sssd-ad.html.en](https://help.ubuntu.com/lts/serverguide/sssd-ad.html.en)
* [https://bitsofwater.com/2018/05/08/join-ubuntu-18-04-to-active-directory/](https://bitsofwater.com/2018/05/08/join-ubuntu-18-04-to-active-directory/)
* [https://community.spiceworks.com/how_to/144127-how-to-authenticate-an-ubuntu-16-04-host-against-an-active-directory-domain](https://community.spiceworks.com/how_to/144127-how-to-authenticate-an-ubuntu-16-04-host-against-an-active-directory-domain)
* [https://web.archive.org/web/20170507073518/http://www.passthebit.com/2017/05/06/join-ubuntu-16-04-to-windows-active-directory-domain/](https://web.archive.org/web/20170507073518/http://www.passthebit.com/2017/05/06/join-ubuntu-16-04-to-windows-active-directory-domain/)
